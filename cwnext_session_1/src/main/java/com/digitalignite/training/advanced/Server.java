package com.digitalignite.training.advanced;

import java.util.Collections;
import java.util.List;

import com.digitalignite.datahandler.handlers.impl.DataHandler;
import com.digitalignite.datahandler.module.entity.processors.IProcessorsClass;
import com.digitalignite.datahandler.module.entity.processors.ProcessorFactory;
import com.digitalignite.datahandler.module.entity.util.RequestContext;
import com.digitalignite.datahandler.processors.Processor;

/**
 * Creates a list of processors from an {@link IProcessorsClass} class then
 * registers them with the handler and starts the handler
 *
 * @author jesposito
 * @version 8/21/18
 */

public class Server {

	public static void main(String[] args) {

		DataHandler handler = new DataHandler();

		List<Processor> processors = new ProcessorFactory(Server::parseToken).createProcessors(new Processors());

		System.err.println("size: " + processors.size());

		handler.register(processors);
		handler.start();

	}

	private static RequestContext parseToken(String token) {

		// make a request to the auth server to decrypt token
		// here we are returning a request context as if it was decrypted

		RequestContext rc = new RequestContext();
		rc.setRoles(Collections.singletonList("ROLE_ADMI2N"));

		return rc;

	}
}

package com.digitalignite.training.simple;

import java.util.UUID;

import com.digitalignite.datahandler.handlers.impl.DataHandler;
import com.digitalignite.datahandler.processors.Processor;
import com.digitalignite.training.User;
import com.digitalignite.training.Params;

/**
 * Processor to provide sample data
 *
 * @author jesposito
 * @version 8/21/18
 */

public class Server {

	public static void main(String[] args) {

		DataHandler handler = new DataHandler();

		Processor processor1 = Processor.create("user1", Params.class, (params) -> {

			User result = new User();

			result.setId(params.getId());
			result.setFirstName(UUID.randomUUID().toString());
			result.setLastName(UUID.randomUUID().toString());

			return result;

		});

		Processor<Params> processor2 = new Processor<Params>("user2", Params.class) {

			@Override
			public Object respond(Params params) {

				User result = new User();

				result.setId(params.getId());
				result.setFirstName(UUID.randomUUID().toString());
				result.setLastName(UUID.randomUUID().toString());

				return result;

			}
		};

		handler.register(processor1);
		handler.register(processor2);
		handler.start();

	}

}

package com.digitalignite.training.advanced;

import com.digitalignite.datahandler.module.entity.handlers.impl.EntityRequester;
import com.digitalignite.datahandler.module.entity.results.ActionResult;

/**
 * Client that uses {@link EntityRequester}
 *
 * @author jesposito
 * @version 8/21/18
 */

public class Client {

	public static void main(String[] args) {

		String token = "234j329j8evaf3e23io";

		EntityRequester requester = new EntityRequester(token);

		ActionResult result = requester.deleteOne("user", 32L);

		System.err.println(result);

		System.err.println(result.getOperationPerformed());
		System.err.println(result.getOperationResult());
		System.err.println(result.getEntityId());
	}

}

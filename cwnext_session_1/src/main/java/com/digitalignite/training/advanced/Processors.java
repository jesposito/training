package com.digitalignite.training.advanced;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import com.digitalignite.datahandler.module.entity.processors.IEntityProcessor;
import com.digitalignite.datahandler.module.entity.processors.IProcessorsClass;
import com.digitalignite.datahandler.module.entity.processors.annotations.Processor;
import com.digitalignite.datahandler.module.entity.processors.annotations.RolesAllowed;
import com.digitalignite.datahandler.module.entity.processors.annotations.Unsupported;
import com.digitalignite.datahandler.module.entity.results.ActionResult;
import com.digitalignite.datahandler.module.entity.util.RequestContext;
import com.digitalignite.training.Params;
import com.digitalignite.training.User;

/**
 * Class with more than one processor
 *
 * @author jesposito
 * @version 8/21/18
 */

public class Processors implements IProcessorsClass {

	@Override
	public String getName() {
		return "user";
	}

	@Processor(postfix = "one")
	public User one(Params params, RequestContext rc) {

		User result = new User();
		result.setId(params.getId());
		result.setFirstName(UUID.randomUUID().toString());

		return result;
	}

	@Processor(postfix = "list")
	public List<User> getAll(Params params, RequestContext rc) {

		User result = new User();
		result.setId(params.getId());
		result.setFirstName(UUID.randomUUID().toString());

		return Collections.singletonList(result);
	}

	@RolesAllowed("ROLE_ADMIN")
	@Processor(postfix = IEntityProcessor.ONE_DELETE_POSTFIX)
	@Unsupported
	public ActionResult delete(Long id, RequestContext rc) {

		// delete our item
		// SupportService.delete(params.getId());

		// return the id if successful
		return ActionResult.deleted(id);
	}
}
package com.digitalignite.training.simple;


import com.digitalignite.datahandler.handlers.impl.DataHandler;
import com.digitalignite.training.User;
import com.digitalignite.training.Params;

/**
 * Simple client that requests data
 *
 * @author jesposito
 * @version 8/21/18
 */

public class Client {

	public static void main(String[] args) {

		DataHandler handler = new DataHandler();

		Params params = new Params();
		params.setId(2342);

		User result = handler.requestOne("user1", User.class, params);

		System.err.println(result);

	}
}

package com.digitalignite.training;

/**
 * file desc
 *
 * @author jesposito
 * @version 8/21/18
 */

public class Params {
	public long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Params{" +
				"id=" + id +
				'}';
	}
}
